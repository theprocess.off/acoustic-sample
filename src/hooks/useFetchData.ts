import { useState, useCallback } from "react";

interface useFetchDataProps<T> {
    url: string;
}

interface stateResponse<T> {
    data: T | null;
    error: boolean;
    isLoading: boolean;
}

const useFetchData  = <T extends any>(config: useFetchDataProps<T>) => {
    const [response, setResponse] = useState<stateResponse<T>>({data: null, error: false, isLoading: true});

    const fetchData = useCallback(async () => {
        
        const response = await fetch(config.url);
        const body = await response.json();
        setResponse({data: body, error: false, isLoading: false});
        
    },[]);

    return [response, fetchData] as const;
}

export default useFetchData;