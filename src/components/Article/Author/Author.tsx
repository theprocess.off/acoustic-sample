import { Box, Container, Typography } from '@mui/material';
import React from 'react';

import AccountCircleIcon from '@mui/icons-material/AccountCircle';

type AuthorProps = {
    authorName: string;
}

const Author = (props: AuthorProps) => {
    return (
        <Container>
            <Box marginY={2} display='flex' alignItems='center' justifyContent='center'>
                <Box>
                    <AccountCircleIcon color='disabled' />
                </Box>
                <Box>
                    <Typography variant='caption' color='GrayText'>
                        Created by: {props.authorName}
                    </Typography>
                </Box>
            </Box>
        </Container>
    );
};

export default Author;