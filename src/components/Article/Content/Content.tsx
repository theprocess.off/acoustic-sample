import { Box, Container, Typography } from '@mui/material';
import React from 'react';

type ContentProps = {
    body: string[];
}

const Content = (props: ContentProps) => {
    console.log(props);
    const { body } = props; 

    return (
        <Container className='Content' >
            {/* Not secure but demo porpuse */}
            {body.map(x => <Typography dangerouslySetInnerHTML={{__html: x}} />)}
        </Container>
    );
};

export default Content;