import { Container, Divider, Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React, { useEffect } from 'react';
import { APIURL } from '../../constants/AppConstants';
import useFetchData from '../../hooks/useFetchData';
import { IArticle } from '../../models/IArticle';
import Cover from '../Cover/Cover';
import Content from './Content/Content';
import MetaInfo from './MetaInfo/MetaInfo';

import Author from './Author/Author';

const Article = () => {
    const [article, fetchArticle] = useFetchData<IArticle>(
        {url: `${APIURL}/api/859f2008-a40a-4b92-afd0-24bb44d10124/delivery/v1/content/db4930e9-7504-4d9d-ae6c-33facca754d1`}
    );

    useEffect(() => {
        fetchArticle();
    }, []);

    return (
        <Container className='Article'>
            {!article.isLoading ?
            <Box padding={5}>
                <Box marginBottom={8}>
                    <Typography variant='h4'  fontFamily={'monospace'}>
                        {article.data?.name}
                    </Typography>
                    <Divider />
                    <Author authorName={article.data!.elements.author.value}/>
                </Box>
                <Grid container spacing={2}>
                        <Grid item xs={12} md={7}>     
                            <Cover articleImage={article.data!.elements.mainImage} />
                            <MetaInfo 
                                created={article.data!.created}
                                lastUpdate={article.data!.lastModified}
                                revision={article.data!.rev}
                            />
                        </Grid>
                        <Grid item xs={12} md={5}>
                            <Typography variant='h4' fontFamily={'monospace'}>
                                {article.data?.elements.heading.value}
                            </Typography>
                            <Divider />
                            <Content body={article.data!.elements.body.values} />
                        </Grid>
                </Grid>
            </Box> : <Typography>Loading...</Typography>}
        </Container>
    );
};

export default Article;