import { Container, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { format } from 'date-fns';
import React from 'react';


type MetaInfoProps = {
    created: Date;
    revision: string;
    lastUpdate: Date;
}

const MetaInfo = (props: MetaInfoProps) => {
    const {created, revision, lastUpdate} = props;
    return (
        <Container className='MetaInfo'>
            <Box display='flex' flexDirection='column' justifyContent='center' alignItems='center'>

                <Typography variant='caption' color='GrayText'>
                    Published: {format(new Date(created), 'MMM/dd/yyyy') }
                </Typography>    

                <Typography variant='caption' color='GrayText'>
                    Last Update: {format(new Date(lastUpdate), 'MMM/dd/yyyy') }
                </Typography>
                    
                <Typography variant='caption' color='GrayText'>
                    Rev: {revision}
                </Typography>
            </Box>
        </Container>
    );
};

export default MetaInfo;