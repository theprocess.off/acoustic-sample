import { Box } from '@mui/system';
import React from 'react';

const Footer = () => {
    return (
        <Box 
            position={'fixed'}
            bottom={0}
            width='100%'
            right={0}
            padding={1}
            bgcolor='#12515b'
            color='#FFF'
            textAlign='right'
        >
            Made with 💙 by Carlos Arenas
        </Box>
    );
};

export default Footer;