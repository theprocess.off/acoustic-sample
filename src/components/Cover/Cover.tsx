import { Container } from '@mui/material';
import React from 'react';
import { APIURL } from '../../constants/AppConstants';
import { IArticleMainImage } from '../../models/IArticle';

type CoverProps = {
    articleImage: IArticleMainImage;
}


const Cover = (props: CoverProps) => {
    const {value, elementType, typeRef} = props.articleImage;
    return (
        <Container className='cover' >
            <img src={`${APIURL}/${value.leadImage.renditions.card!.url}`} width="100%" height='auto' />
        </Container>
    );
};

export default Cover;