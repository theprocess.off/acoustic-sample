export enum ArticleTypesEnum {
    Article,
    Page,
    Content
}

export enum ArticleStatusEnum {
    ready,
    draft,
    deleted,
}

export interface IArticleThumbnail {
    id: string;
    url: string;
}

export interface IArticleElementType {
    elementType: string;
    value: string;
}

export interface IArticleCard {
    source: string;
    url: string;
}

export interface IArticleMainImage {
    elementType: string,
    value: {
        leadImage: {
            mode: string;
            profiles: [],
            renditions: {
                card: IArticleCard;
            },
            asset: {},
            elementType: string;
            url: string;
        },
    },
    typeRef: {
        id: string;
    }
}

export interface IArticleElements {
    heading: IArticleElementType;
    author: IArticleElementType;
    body: {
        values: string[]
    };
    date: IArticleElementType;
    mainImage: IArticleMainImage;
}

export interface IArticle {
    reviewHistory: [];
    keywords: [];
    libraryId: string;
    creatorId: string;
    description: string;
    type: ArticleTypesEnum;
    locale: string;
    lastModifierId: string;
    links: {};
    id: string;
    systemModified: Date;
    rev: string;
    thumbnail: {};
    kind: [];
    created: Date;
    classification: ArticleTypesEnum;
    tags: [];
    elements: IArticleElements;
    name: string;
    typeId: string;
    lastModified: Date;
    status: ArticleStatusEnum;
}

