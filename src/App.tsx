import { useState } from 'react';
import './App.css';
import { Button, Container } from '@mui/material';
import Article from './components/Article/Article';
import Footer from './components/Footer/Footer';

function App() {

  return (
    <Container className='App'>
      <Article />
      <Footer />
    </Container>
  )
}

export default App
