#  Acoustic - Sample

## Instructions
    - Using React, retrieve the content item(e.g. using the URL) and render the article elementswithin your single page application.
    - You are free to design layout/styles and libraries in your SPA.
    - Make it look simple, clean and easy to consume by a user.
##

#
## Technologies
    - Vite 
    - MUI
    - React
    - Typescript
##

## Required to Run this Project:
 * Nodejs
 * Node Package Manager
 * Docker (Optional)
##

## Run Development Environment:
```
 # Install Dependencies
 npm install 
 # Run Project
 npm start
```
##

## Run Docker Container:
```
docker-compose up -d --build
```
##

## Stop Docker Container:
```
docker-compose stop
```

## Build Dist Version:
```
npm run build
```

## Run Jest tests: 
```
npm run test
```